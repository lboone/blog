class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validate_presence_of :post_id
	validate_presence_of :body
end
